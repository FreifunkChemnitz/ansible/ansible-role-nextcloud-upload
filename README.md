# ansible role for nextcloud-upload

This role can be used to upload files to a nextcloud file drop.

## required variables
* dest_file
* nextcloud_domain
* nextcloud_password
* nextcloud_username
* src_file

## license

This work is licensed under the Unlicense.
